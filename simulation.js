function colorBall(ball){
    if(ball == 0)
    {
        return "Green";
    }
    else if(ball > 7)
    {
        return "Black";
    }
    else
    {
        return "Red";
    }
}

function lastBallNoGreenInverse(n){
    var i;

    for(i=n-1;i>=0;i--) {
        if (document.getElementsByClassName("td-val")[i] != null) {
            if (colorBall(document.getElementsByClassName("td-val")[i].innerHTML) == "Red") {
                return "Black";
            }
            else if (colorBall(document.getElementsByClassName("td-val")[i].innerHTML) == "Black") {
                return "Red";
            }
        }
    }
}




var rainbow  = {
    numberWin: 0,
    numberLose: 0,
    loseStreak: 0,
    maxLoseStreak: 0
};

var Alternate = {
    numberWin: 0,
    numberLose: 0,
    loseStreak: 0,
    maxLoseStreak: 0
};


var i;


for(i=1;i<=2000;i++){

    if(document.getElementsByClassName("td-val")[i] != null) {
        if (colorBall(document.getElementsByClassName("td-val")[i].innerHTML) != "Green") {

            if (colorBall(document.getElementsByClassName("td-val")[i].innerHTML) == lastBallNoGreenInverse(i)) {
                rainbow.numberWin++;
                rainbow.loseStreak = 0;
            }
            else {
                rainbow.numberLose++;
                rainbow.loseStreak++;

                if (rainbow.loseStreak > rainbow.maxLoseStreak) {
                    rainbow.maxLoseStreak = rainbow.loseStreak;
                }

            }
        }
    }
}


console.log("Nombre de win:" + rainbow.numberWin);
console.log("Nombre de lose:" + rainbow.numberLose);
console.log("Plus grande lose streak:" + rainbow.maxLoseStreak);