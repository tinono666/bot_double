var bannerDIV = document.getElementById('banner');
var balanceDIV = document.getElementById('balance');

var redBtnDIV = document.getElementsByClassName('btn btn-danger btn-lg  btn-block betButton');
//var redTotalDIV = document.getElementsByClassName('pull-right total')[0];
var redTotal = parseInt(document.getElementsByClassName('pull-right total')[0].innerHTML);

var greenBtnDIV = document.getElementsByClassName('btn btn-success btn-lg  btn-block betButton');
//var greenTotalDIV = document.getElementsByClassName('pull-right total')[1];
var greenTotal = parseInt(document.getElementsByClassName('pull-right total')[1].innerHTML);

var blackBtnDIV = document.getElementsByClassName('btn btn-inverse btn-lg  btn-block betButton');
//var blackTotalDIV = document.getElementsByClassName('pull-right total')[2];
var blackTotal = parseInt(document.getElementsByClassName('pull-right total')[2].innerHTML);

var totalBet;

var amountDIV = document.getElementById('betAmount');

var statDiv = document.createElement('div');
statDiv.id = "statTab";

var tab1Div = document.getElementById("tab1");
tab1Div.appendChild(statDiv);

var bettingTime = 15; //the time it waits until it sets the bet
var refreshTime = 1000; // the time the loop waits until it refreshes
var betAmount = 10; //amount of coins the bot bets
var loseStreak = 0;
var betWithMasses = true; //bot checks which color has the highest total bet. If true it bets on this one. if not it bets on the opposite.

var betted = false; //misc variable to avoid double bets
var pause = true;

document.getElementsByClassName("form-group")[2].innerHTML =  "" +
    "<div class=\"row\">" +
    "" +
    "<div class=\"col-xs-2\">" +
    "" +
    "<label>Bet Mode:</label>" +
    "<select class=\"form-control\" id=\"betMode\">"+
    "<option value=\"Pause\">Pause</option>"+
    "<option value=\"Masses\">Masses</option>"+
    "<option value=\"Red\">Red</option>"+
    "<option value=\"Black\">Black</option>"+
    "<option value=\"Alternate\">Alternate</option>"+
    "<option value=\"Rainbow\">Rainbow</option>"+
    "<option value=\"Experimental\">Experimental</option>"+
    "</select>" +
    "" +
    "</div>" +
    "" +
    "</div>";

var betMode = "Pause";

var numberTrain = 3;

var newBalance;

var numberWin = 0;
var numberWin2;
var numberLose = 0;
var biggestLoseStreak = 0;

var lastBall = document.getElementsByClassName("ball")[9].getAttribute("data-rollid");
var lastBallValue = document.getElementsByClassName("ball")[9].innerHTML;
var newBall;

var lastBetcolor = "nothing";
var lastResult = "nothing";

var statsDiv = document.getElementById("statTab");

statsDiv.innerHTML = " <div class=\"table-responsive\"> <h3>Bot statistics</h3> " +
    "<table class=\"table\"> " +
    "<tbody> " +

    "<tr> " +
    "<td>Number of wins: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Number of loses: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Biggest lose streak: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Coins earned/lost: </td> " +
    "<td>0</td>  " +
    "</tr> " +

    "<tr> " +
    "<td>Last bet/result: </td> " +
    "<td>" + lastBetcolor + "/" + lastResult + "</td>  " +
    "</tr> " +

    "</tbody> " +
    "</table> " +
    "</div>";

var startingCoins;
var oldBalance;


function mainLoop () { //recursive loop to call main func
    setTimeout(function () {
        mainFunc(); //call main func
        mainLoop(); //call itself
    }, refreshTime); //wait

}


setTimeout(function () {
    startingCoins =  parseInt(balanceDIV.innerHTML);
    oldBalance = startingCoins;
    mainLoop();
}, 2000);




//call loop once to get it started
function mainFunc(){
    updateValues(); //read total values
    if (bannerDIV.innerHTML.indexOf('Rolling in ') == 0) { //checks the status
        var timeString =  bannerDIV.innerHTML.substring(11,99); //parse the time
        timeString = timeString.substring(0,timeString.length-3);//remove the ... from time
        if (parseFloat(timeString) <= 22 && parseFloat(timeString) >= bettingTime){ //avoid multiple betting
            betted = false;
        }
        if (parseFloat(timeString) <= bettingTime){ //if the time is under the betting time
            if (betted == false && betMode != "Pause") {
                bet(); // do the betting
            }
        }
    }
}

function bet(){

    console.log("Red:" + redTotal + " Green:" + greenTotal + " Black:" + blackTotal + " betWithMasses:"+betWithMasses); //print out the current values
    amountDIV.value =  betAmountF(); //set the amount
    switch(betMode)
    {
        case "Masses":

            if (redTotal > greenTotal && redTotal > blackTotal){ //if red is the biggest value

                if (betWithMasses){
                    redBtnDIV[0].click();
                    lastBetcolor= "Red";
                }
                else{
                    blackBtnDIV[0].click();
                    lastBetcolor = "Black";
                }

            } else if(greenTotal > redTotal && greenTotal > blackTotal){//if green is the biggest value
                greenBtnDIV[0].click();//clicks on green
                lastBetcolor = "Green";

            } else if(blackTotal > redTotal && blackTotal > greenTotal) {//if black is the biggest value

                if (betWithMasses){
                    blackBtnDIV[0].click();
                    lastBetcolor = "Black";
                }
                else {
                    redBtnDIV[0].click();
                    lastBetcolor= "Red";
                }//checks if betWithMasses is toggled and clicks on the button
            } else {
                lastBetcolor = "none";//if something is equal
            }

            break;

        case "Red":

            if(blackTrain()) {
                blackBtnDIV[0].click();
                lastBetcolor = "Black";
            }
            else {
                redBtnDIV[0].click();
                lastBetcolor= "Red";
            }
            break;

        case "Black":

            if(redTrain()) {
                redBtnDIV[0].click();
                lastBetcolor= "Red";
            }
            else {
                blackBtnDIV[0].click();
                lastBetcolor = "Black";
            }
            break;

        case "Alternate":

            if(lastBetcolor == "Black") {
                redBtnDIV[0].click();
                lastBetcolor = "Red";
            }
            else{
                blackBtnDIV[0].click();
                lastBetcolor = "Black";
            }

            break;

        case "Rainbow":

            if(lastBallNoGreen() == "Black"){
                if(blackTrain()){
                    blackBtnDIV[0].click();
                    lastBetcolor = "Black";
                }
                else {
                    redBtnDIV[0].click();
                    lastBetcolor = "Red";
                }
            }
            else if(lastBallNoGreen() == "Red"){
                if(redTrain()){
                    redBtnDIV[0].click();
                    lastBetcolor = "Red";
                }
                else {
                    blackBtnDIV[0].click();
                    lastBetcolor = "Black";
                }
            }

            break;

        case "Experimental":

            if(statBalls() == "Black"){
                blackBtnDIV[0].click();
                lastBetcolor = "Black";
            }
            else if(statBalls() == "Red"){
                redBtnDIV[0].click();
                lastBetcolor = "Red";
            }
            else{
                greenBtnDIV[0].click();
                lastBetcolor = "Green";
            }
            break;

    }

    lastResult = "Wait";

    betted = true;//avoid betting multiple times

}

function updateValues(){ //read total values
    redTotal = parseInt(document.getElementsByClassName('pull-right total')[0].innerHTML);
    greenTotal = parseInt(document.getElementsByClassName('pull-right total')[1].innerHTML);
    blackTotal = parseInt(document.getElementsByClassName('pull-right total')[2].innerHTML);

    pourcentageTotal();

    betMode = document.getElementById("betMode").value;

    newBall = document.getElementsByClassName("ball")[9].getAttribute("data-rollid");

    if(newBall != lastBall){
        lastBall = newBall;
        lastBallValue = document.getElementsByClassName("ball")[9].innerHTML;
        newBalance = document.getElementById('balance').innerHTML;
        if(betMode != "Pause"){
            win();
        }
        amountDIV.value =  betAmountF(); //set the amount
        oldBalance = document.getElementById('balance').innerHTML;
    }

    getTotalEarnings();

}

function getTotalEarnings(){//calc your earnings
    if(numberWin > 0)
        numberWin2 = numberWin;
    else
        numberWin2 = 0;

    statsDiv.innerHTML = " <div class=\"table-responsive\"> <h3>Bot statistics</h3> " +
        "<table class=\"table\"> " +
        "<tbody> " +

        "<tr> " +
        "<td>Number of wins: </td> " +
        "<td>" + numberWin2 + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Number of loses: </td> " +
        "<td>" + numberLose + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Biggest lose streak: </td> " +
        "<td>" + biggestLoseStreak + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Coins earned/lost: </td> " +
        "<td>"+(parseInt(document.getElementById('balance').innerHTML - startingCoins)) + "</td>  " +
        "</tr> " +

        "<tr> " +
        "<td>Last bet/result: </td> " +
        "<td>" + lastBetcolor + "/" + lastResult + "</td>  " +
        "</tr> " +

        "</tbody> " +
        "</table> " +
        "</div>";

}


function win(){

    if(oldBalance == newBalance) {
        console.log("Something wrong happened or the bot is waiting");
    } else if(lastBetcolor != colorBall(lastBallValue)) {
        console.log("its a lose");
        lastResult = "lost";
        numberLose++;
        loseStreak++;
        if(loseStreak > biggestLoseStreak) {
            biggestLoseStreak++
        }
    } else {
        console.log("its a win");
        lastResult = "won";
        loseStreak = 0;
        numberWin++;
    }
}


function betAmountF(){
    return betAmount*Math.pow(2,loseStreak);
}


function pourcentageTotal(){
    totalBet = redTotal + greenTotal + blackTotal;
    document.getElementsByClassName('btn btn-danger btn-lg  btn-block betButton')[0].innerHTML = Math.round(redTotal/totalBet*100) + "%";
    document.getElementsByClassName('btn btn-success btn-lg  btn-block betButton')[0].innerHTML = Math.round(greenTotal/totalBet*100) + "%";
    document.getElementsByClassName('btn btn-inverse btn-lg  btn-block betButton')[0].innerHTML = Math.round(blackTotal/totalBet*100) + "%";

}



function colorBall(ball){
    if(ball == 0)
    {
        return "Green";
    }
    else if(ball > 7)
    {
        return "Black";
    }
    else
    {
        return "Red";
    }
}


function redTrain(){
    var i;
    var count = 0;
    for(i=10-numberTrain; i<=9; i++) {
        if (colorBall(document.getElementsByClassName("ball")[i].innerHTML) == "Red") {
            count++;
        }
    }
    return (count == numberTrain);
}

function blackTrain(){
    var i;
    var count = 0;
    for(i=10-numberTrain; i<=9; i++) {
        if (colorBall(document.getElementsByClassName("ball")[i].innerHTML) == "Black") {
            count++;
        }
    }
    return (count == numberTrain);
}


function lastBallNoGreen(){
    var i;

    for(i=9;i>=0;i--){
        if(colorBall(document.getElementsByClassName("ball")[i].innerHTML) == "Red"){
            return "Red";
        }
        else if(colorBall(document.getElementsByClassName("ball")[i].innerHTML) == "Black"){
            return "Black";
        }
    }
}


function statBalls(){
    var i;
    var count = 0;
    for(i=0; i<=9; i++) {
        if (colorBall(document.getElementsByClassName("ball")[i].innerHTML) == "Black") {
            count++;
        }
        else{
            count--;
        }
    }

    if(count > 0){
        return "Red";
    }
    else if(count < 0){
        return "Black";
    }
    else{
        return "Green";
    }
}