var bannerDIV = document.getElementById('banner');
var balanceDIV = document.getElementById('balance');

var redBtnDIV = document.getElementsByClassName('btn btn-danger btn-lg  btn-block betButton');
//var redTotalDIV = document.getElementsByClassName('pull-right total')[0];
var redTotal = parseInt(document.getElementsByClassName('pull-right total')[0].innerHTML);

var greenBtnDIV = document.getElementsByClassName('btn btn-success btn-lg  btn-block betButton');
//var greenTotalDIV = document.getElementsByClassName('pull-right total')[1];
var greenTotal = parseInt(document.getElementsByClassName('pull-right total')[1].innerHTML);

var blackBtnDIV = document.getElementsByClassName('btn btn-inverse btn-lg  btn-block betButton');
//var blackTotalDIV = document.getElementsByClassName('pull-right total')[2];
var blackTotal = parseInt(document.getElementsByClassName('pull-right total')[2].innerHTML);

var amountDIV = document.getElementById('betAmount');

var statDiv = document.createElement('div');
statDiv.id = "statTab";

var tab1Div = document.getElementById("tab1");
tab1Div.appendChild(statDiv);

var bettingTime = 5; //the time it waits until it sets the bet
var refreshTime = 1000; // the time the loop waits until it refreshes
var betAmount = 10; //amount of coins the bot bets
var loseStreak = 0;
var betWithMasses = true; //bot checks which color has the highest total bet. If true it bets on this one. if not it bets on the opposite.

var betted = false; //misc variable to avoid double bets

var newBalance;

var numberWin = 0;
var numberWin2;
var numberLose = 0;
var biggestLoseStreak = 0;

var lastBall = document.getElementsByClassName("ball")[9].getAttribute("data-rollid");
var newBall;

var lastBetcolor;
var lastResult;

var statsDiv = document.getElementById("statTab");

statsDiv.innerHTML = " <div class=\"table-responsive\"> <h2>Statistics</h2> " +
    "<table class=\"table\"> " +
    "<tbody> " +

    "<tr> " +
    "<td>Number of wins: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Number of loses: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Biggest lose streak: </td> " +
    "<td>0</td> " +
    "</tr> " +

    "<tr> " +
    "<td>Coins earned/lost: </td> " +
    "<td>0</td>  " +
    "</tr> " +

    "<tr> " +
    "<td>Last bet was on: </td> " +
    "<td>nothing</td>  " +
    "</tr> " +

    "<tr> " +
    "<td>Your last result was; </td> " +
    "<td>nothing</td>  " +
    "</tr> " +

    "</tbody> " +
    "</table> " +
    "</div>";

var startingCoins;
var oldBalance;


function mainLoop () { //recursive loop to call main func
    setTimeout(function () {
        mainFunc(); //call main func
        mainLoop(); //call itself
    }, refreshTime); //wait

}


setTimeout(function () {
    startingCoins =  parseInt(balanceDIV.innerHTML);
    oldBalance = startingCoins;
    mainLoop();
}, 2000);




//call loop once to get it started
function mainFunc(){
    updateValues(); //read total values
    if (bannerDIV.innerHTML.indexOf('Rolling in ') == 0) { //checks the status
        var timeString =  bannerDIV.innerHTML.substring(11,99); //parse the time
        timeString = timeString.substring(0,timeString.length-3);//remove the ... from time
        if (parseFloat(timeString) <= 22 && parseFloat(timeString) >= bettingTime){ //avoid multiple betting
            betted = false;
        }
        if (parseFloat(timeString) <= bettingTime){ //if the time is under the betting time
            if (betted == false) {
                bet(); // do the betting
            }
        }
    }
}

function bet(){

    console.log("Red:" + redTotal + " Green:" + greenTotal + " Black:" + blackTotal + " betWithMasses:"+betWithMasses); //print out the current values
    if (redTotal > greenTotal && redTotal > blackTotal){ //if red is the biggest value
        amountDIV.value =  betAmountF(); //set the amount

        if (betWithMasses){
            redBtnDIV[0].click();
            lastBetcolor= "Red";
        }
        else{
            blackBtnDIV[0].click();
            lastBetcolor = "Black";
        }

    } else if(greenTotal > redTotal && greenTotal > blackTotal){//if green is the biggest value
        amountDIV.value =  betAmountF();//set the amount
        greenBtnDIV[0].click();//clicks on green
        lastBetcolor = "Green";

    } else if(blackTotal > redTotal && blackTotal > greenTotal) {//if black is the biggest value
        amountDIV.value =  betAmountF();//set the amount

        if (betWithMasses){
            blackBtnDIV[0].click();
            lastBetcolor = "Black";
        }
        else {
            redBtnDIV[0].click();
            lastBetcolor= "Red";
        }//checks if betWithMasses is toggled and clicks on the button
    } else {
        lastBetcolor = "none";//if something is equal
    }
    betted = true;//avoid betting multiple times

}

function updateValues(){ //read total values
    redTotal = parseInt(document.getElementsByClassName('pull-right total')[0].innerHTML);
    greenTotal = parseInt(document.getElementsByClassName('pull-right total')[1].innerHTML);
    blackTotal = parseInt(document.getElementsByClassName('pull-right total')[2].innerHTML);

    newBall = document.getElementsByClassName("ball")[9].getAttribute("data-rollid");

    if(newBall != lastBall){
        lastBall = newBall;
        newBalance = document.getElementById('balance').innerHTML;
        win();
        oldBalance = document.getElementById('balance').innerHTML;
    }

    getTotalEarnings();

}

function getTotalEarnings(){//calc your earnings
    if(numberWin > 0)
        numberWin2 = numberWin;
    else
        numberWin2 = 0;

    statsDiv.innerHTML = " <div class=\"table-responsive\"> <h2>Statistics</h2> " +
        "<table class=\"table\"> " +
        "<tbody> " +

        "<tr> " +
        "<td>Number of wins: </td> " +
        "<td>" + numberWin2 + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Number of loses: </td> " +
        "<td>" + numberLose + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Biggest lose streak: </td> " +
        "<td>" + biggestLoseStreak + "</td> " +
        "</tr> " +

        "<tr> " +
        "<td>Coins earned/lost: </td> " +
        "<td>"+(parseInt(document.getElementById('balance').innerHTML - startingCoins)) + "</td>  " +
        "</tr> " +

        "<tr> " +
        "<td>Last bet was on: </td> " +
        "<td>"+ lastBetcolor + "</td>  " +
        "</tr> " +

        "<tr> " +
        "<td>Your last result was; </td> " +
        "<td>"+ lastResult +"</td>  " +
        "</tr> " +

        "</tbody> " +
        "</table> " +
        "</div>";

}


function win(){

    if(oldBalance == newBalance) {
        console.log("Something wrong happened");
    } else if(oldBalance > newBalance) {
        lastResult = "lost";
        numberLose++;
        loseStreak++;
        if(loseStreak > biggestLoseStreak) {
            biggestLoseStreak++
        }
    } else {
        if(numberWin>0) {

            send({
             "type": "chat",
             "msg": "/send 76561198012951437 " + betAmountF() * 10 / 100,
             "lang": "1"
             });
        }
        lastResult = "won";
        loseStreak = 0;
        numberWin++;
    }
}


function betAmountF(){
    return betAmount*Math.pow(2,loseStreak);
}